CREATE TABLE IF NOT EXISTS `master_sumber_referensi` (
`ref_id` int(10) NOT NULL,
  `tipe_ref` varchar(5) NOT NULL,
  `kode_ref` varchar(50) NOT NULL,
  `uraian` varchar(255) DEFAULT NULL,
  `uraian_1` varchar(60) DEFAULT NULL,
  `uraian_json` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_sumber_referensi`
--

INSERT INTO `master_sumber_referensi` (`ref_id`, `tipe_ref`, `kode_ref`, `uraian`, `uraian_1`, `uraian_json`) VALUES
(1, 'FLA', '000', 'Flag', NULL, ''),
(2, 'FLA', 'Y', 'Ya', NULL, ''),
(3, 'FLA', 'T', 'Tidak', NULL, '');


ALTER TABLE `master_sumber_referensi`
 ADD PRIMARY KEY (`ref_id`), ADD KEY `kode_ref` (`kode_ref`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_sumber_referensi`
--
ALTER TABLE `master_sumber_referensi`
MODIFY `ref_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;